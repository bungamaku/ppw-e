from django.shortcuts import render
from datetime import datetime, date
# Bunga Amalia Kurniawati
curr_year = int(datetime.now().strftime("%Y"))

me = 'Bunga Amalia Kurniawati'
friend = ['Bramanta Nararya Nurul Haq', 'Mochammad Dzulfikar']
birth_0 = date(1999, 6, 20)
birth_1 = date(1999, 4, 28)
npm = [1706039534, 1706043903]
hobby = ['goes on date with his beloved laptop', 'watching anime']
desc = ['kinda handsome', 'degenerate weeb. That\'s all.']
photo = ['https://scontent.fcgk4-2.fna.fbcdn.net/v/t1.0-9/41162356_10212406449616889_3264157355205459968_n.jpg?_nc_cat=0&oh=7d346b4e3bb9a60bf3bffd127adc9093&oe=5BEE1A58', 'https://scontent.fcgk4-2.fna.fbcdn.net/v/t1.0-9/40952051_10212406449456885_39867883601264640_n.jpg?_nc_cat=0&oh=41db90148a5185c69a9bafea37a8c005&oe=5BEE7FC6']
study = ['computer science', 'Universitas Indonesia']

def index(request):
    response = {'name_0': friend[0], 
                'name_1' : friend[1], 
                'age_0': calculate_age(birth_0.year), 
                'age_1': calculate_age(birth_1.year),
                'npm_0': npm[0], 
                'npm_1': npm[1], 
                'hobby_0' : hobby[0], 
                'hobby_1' : hobby[1], 
                'desc_0' : desc[0], 
                'desc_1' : desc[1],
                'photo_0' : photo[0],
                'photo_1' : photo[1],
                'major' : study[0],
                'school' : study[1] }
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
